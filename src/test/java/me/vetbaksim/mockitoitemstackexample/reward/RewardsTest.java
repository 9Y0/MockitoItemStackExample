package me.vetbaksim.mockitoitemstackexample.reward;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.ItemFactory;
import org.bukkit.inventory.meta.ItemMeta;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.legacy.PowerMockRunner;

@RunWith(PowerMockRunner.class)
/* Because we're mocking Bukkit static, we need to prepare it. */
@PrepareForTest({Bukkit.class})
public class RewardsTest {

	@Test
	public void rewardsNameTestWithMockito() {
		PowerMockito.mockStatic(Bukkit.class);
		ItemFactory itemFactory = mock(ItemFactory.class);
		ItemMeta itemMeta = mock(ItemMeta.class);

		when(Bukkit.getItemFactory()).thenReturn(itemFactory);
		when(itemFactory.getItemMeta(any(Material.class))).thenReturn(itemMeta);

		Assert.assertEquals("Reward #1", Rewards.REWARD_ONE.getName());
	}

	/**
	 * This test doesn't work because when the Rewards are initializing, a
	 * ItemStack gets created. Because the server is not running, this will
	 * throw an exception. Because the ItemStack is not important in this test,
	 * we can use Mockito and mock it.
	 */
	@Test
	public void rewardsNameTestWithoutMockito() {
		// Assert.assertEquals("Reward #1", Rewards.REWARD_ONE.getName());
	}

}