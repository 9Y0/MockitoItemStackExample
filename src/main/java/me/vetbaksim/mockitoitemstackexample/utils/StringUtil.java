package me.vetbaksim.mockitoitemstackexample.utils;

import org.bukkit.ChatColor;

public class StringUtil {

	public static String color(String input) {
		return ChatColor.translateAlternateColorCodes('&', input);
	}

}
