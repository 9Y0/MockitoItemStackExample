package me.vetbaksim.mockitoitemstackexample.utils;

import java.util.Arrays;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ItemStackBuilder {

	private ItemStack item = null;

	public ItemStackBuilder(Material material) {
		this.item = new ItemStack(material);
	}

	public ItemStackBuilder withName(String name) {
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(StringUtil.color(name));
		item.setItemMeta(meta);
		return this;
	}

	public ItemStackBuilder withLore(String... lore) {
		ItemMeta meta = item.getItemMeta();
		meta.setLore(colorLore(lore));
		item.setItemMeta(meta);
		return this;
	}

	public ItemStack build() {
		return item;
	}

	private List<String> colorLore(String... lore) {
		for (int i = 0; i < lore.length; i++) {
			lore[i] = StringUtil.color(lore[i]);
		}

		return Arrays.asList(lore);
	}

}
