package me.vetbaksim.mockitoitemstackexample;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import me.vetbaksim.mockitoitemstackexample.listeners.PlayerJoinListener;

public class Main extends JavaPlugin {

	@Override
	public void onEnable() {
		Bukkit.getPluginManager().registerEvents(new PlayerJoinListener(), this);
	}

}
