package me.vetbaksim.mockitoitemstackexample.reward;

import me.vetbaksim.mockitoitemstackexample.reward.rewardactions.RewardOneAction;
import me.vetbaksim.mockitoitemstackexample.reward.rewardactions.RewardTwoAction;

public enum Rewards {

	REWARD_ONE("Reward #1", new RewardOneAction()), REWARD_TWO("Reward #2", new RewardTwoAction());

	private String name;
	private RewardAction rewardAction;

	Rewards(String name, RewardAction rewardAction) {
		this.name = name;
		this.rewardAction = rewardAction;
	}

	public String getName() {
		return name;
	}

	public RewardAction getRewardAction() {
		return rewardAction;
	}
}
