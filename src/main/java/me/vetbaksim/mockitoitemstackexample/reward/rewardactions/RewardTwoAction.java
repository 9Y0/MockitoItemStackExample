package me.vetbaksim.mockitoitemstackexample.reward.rewardactions;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import me.vetbaksim.mockitoitemstackexample.reward.RewardAction;
import me.vetbaksim.mockitoitemstackexample.utils.ItemStackBuilder;

public class RewardTwoAction implements RewardAction {

	private final ItemStack item = new ItemStackBuilder(Material.GRASS).withName("Your fantastic reward number two")
			.withLore("Wow this is also cool!!!").build();

	@Override
	public void giveReward(Player player) {
		player.getInventory().addItem(item);
		player.updateInventory();
	}

}
