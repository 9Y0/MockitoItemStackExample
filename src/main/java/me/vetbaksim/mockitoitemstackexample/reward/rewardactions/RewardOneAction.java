package me.vetbaksim.mockitoitemstackexample.reward.rewardactions;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import me.vetbaksim.mockitoitemstackexample.reward.RewardAction;
import me.vetbaksim.mockitoitemstackexample.utils.ItemStackBuilder;

public class RewardOneAction implements RewardAction {

	private final ItemStack item = new ItemStackBuilder(Material.DIRT).withName("Your fantastic reward")
			.withLore("Wow this is so cool!!!").build();

	@Override
	public void giveReward(Player player) {
		player.getInventory().addItem(item);
		player.updateInventory();
	}

}
