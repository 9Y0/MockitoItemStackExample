package me.vetbaksim.mockitoitemstackexample.reward;

import org.bukkit.entity.Player;

public interface RewardAction {

	void giveReward(Player player);

}
