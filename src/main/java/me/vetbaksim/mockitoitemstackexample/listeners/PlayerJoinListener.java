package me.vetbaksim.mockitoitemstackexample.listeners;

import java.util.Random;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import me.vetbaksim.mockitoitemstackexample.reward.Rewards;

public class PlayerJoinListener implements Listener {

	private final Random random = new Random();

	@EventHandler
	public void onJoin(PlayerJoinEvent event) {
		Rewards.values()[random.nextInt(Rewards.values().length)].getRewardAction().giveReward(event.getPlayer());
	}

}
